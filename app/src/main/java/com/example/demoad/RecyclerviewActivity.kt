package com.example.demoad

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView

class RecyclerviewActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_recyclerview)

        val list = findViewById<RecyclerView>(R.id.list)
        list.adapter = Adapter()
    }

    inner class Adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        val NATIVE = 0
        val CONTENT = 1

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            if(viewType == NATIVE) {
                return NativeAdView(LayoutInflater.from(parent.context).inflate(R.layout.item_native_ad, parent, false))
            } else {
                return ContentView(LayoutInflater.from(parent.context).inflate(R.layout.item_content, parent, false))
            }
        }

        override fun getItemCount(): Int {
            return 100
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if(getItemViewType(position) == CONTENT) {
                val contentView = holder as ContentView
                // set data
            }
        }

        override fun getItemViewType(position: Int): Int {
            if(position % 9 == 0 && position != 0) return NATIVE
            return CONTENT
        }

        inner class ContentView(view: View): RecyclerView.ViewHolder(view) {
        }

        inner class NativeAdView(view: View): RecyclerView.ViewHolder(view) {
        }
    }
}