package com.example.demoad

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class NativeActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_native)
    }
}