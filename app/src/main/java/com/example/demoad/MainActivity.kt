package com.example.demoad

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.liilab.library.advert.RewardAdController

class MainActivity : AppCompatActivity(), RewardAdController.OnRewardedAdListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        (application as DemoApp).interAdController.onRequestToLoadAd()
        (application as DemoApp).rewardAdController.onRequestToLoadAd()
        (application as DemoApp).rewardAdController.setOnRewardedAdListener(this)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun startBannerExample(view: View) {
        startActivity(Intent(this, BannerActivity::class.java))
    }
    fun startNativeExample(view: View) {startActivity(Intent(this, NativeActivity::class.java))}
    fun showVideoAd(view: View) {
        val rewardAdController = (application as DemoApp).rewardAdController

        if(rewardAdController.isLoaded()) {
            rewardAdController.show()
        } else {
            if(rewardAdController.isLoadedFailed()) {
                rewardAdController.onRequestToLoadAd()
            }
        }
    }
    fun showFullScreenAd(view: View) {
        val interAdController = (application as DemoApp).interAdController

        interAdController.showInterstitialAd()
    }

    override fun onAdLoaded() {
    }

    override fun onAdLoadFailed() {
        (application as DemoApp).rewardAdController.onRequestToLoadAd()
    }

    override fun onRewarded() {
        // reward user
    }

    override fun onAdClosed() {
        (application as DemoApp).rewardAdController.onRequestToLoadAd()
    }

    fun startReyclerviewExample(view: View) {
        startActivity(Intent(this, RecyclerviewActivity::class.java))
    }
}
