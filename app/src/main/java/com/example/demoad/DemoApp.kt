package com.example.demoad

import android.app.Application
import com.liilab.library.advert.Advert
import com.liilab.library.advert.InterAdController
import com.liilab.library.advert.RewardAdController

class DemoApp: Application() {

    val interAdController: InterAdController by lazy {
        InterAdController(this)
    }

    val rewardAdController: RewardAdController by lazy {
        RewardAdController(this)
    }

    override fun onCreate() {
        super.onCreate()

        Advert.getInstance().init(this)

        interAdController.setAdmobInterstitialAd()
        rewardAdController.setAdmobRewardAd()
    }
}