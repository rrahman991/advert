package com.liilab.library.advert.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.liilab.library.advert.AdListener
import com.liilab.library.advert.BannerAdController
import com.liilab.library.advert.NativeAdController
import com.liilab.library.advert.R
import com.liilab.library.advert.banner.Size
import com.liilab.library.advert.util.DeviceUtil

class PosterAdView: FrameLayout {

    companion object {
        val ADAPTIVE = 0
        val LARGE = 1
        val MEDIUM = 2
    }

    private val adPriority: Int

    private val bannerAdSize: Int

    private val nativeAdSize: Int

    private val enableCallback: Boolean

    private var bannerAdController: BannerAdController? = null

    private var nativeAdController: NativeAdController? = null

    private var admobNativeId: String? = null

    private var admobBannerId: String? = null

    private var adListener: AdListener? = null

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.PosterAdView,
            0, 0).apply {

            try {
                adPriority = getInteger(R.styleable.PosterAdView_adPriority, ADAPTIVE)
                bannerAdSize = getInteger(R.styleable.PosterAdView_bannerSize, ADAPTIVE)
                nativeAdSize = getInteger(R.styleable.PosterAdView_nativeSize, ADAPTIVE)
                enableCallback = getBoolean(R.styleable.PosterAdView_enableCallback, true)
            } finally {
                recycle()
            }
        }

        loadAdmobId(context)

        initView()
    }

    private lateinit var adProgress: ProgressBar
    private lateinit var adFrameLayout: FrameLayout

    private fun initView() {
        val layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        layoutInflater.inflate(R.layout.native_ad_holder, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        adProgress = findViewById(R.id.adProgress)
        adFrameLayout = findViewById(R.id.adFrameLayout)

        populateAd()
    }

    fun adListener(adListener: AdListener?) {
        this.adListener = adListener
    }

    private fun loadAdmobId(context: Context) {
        try {
            this.admobBannerId = context.getApplicationContext().getString(
                context.getApplicationContext().getResources().getIdentifier(
                    "admob_banner_id", "string",
                    context.getApplicationContext().getPackageName()
                )
            )
        } catch (e: Exception) {
        }

        try {
            this.admobNativeId = context.getApplicationContext().getString(
                context.getApplicationContext().getResources().getIdentifier(
                    "admob_native_ad_id", "string",
                    context.getApplicationContext().getPackageName()
                )
            )
        } catch (e: Exception) {
        }
    }

    private fun populateAd() {
        if(adPriority == 0) {
            bannerAdController = BannerAdController(context);
            bannerAdController?.setBannerHolder(this)
            bannerAdController?.setAdmobBannerAd(admobBannerId ?: "", bannerAdSize, adListener)

            bannerAdController?.loadBannerAds()
        } else {
            val bannerAdController2 = BannerAdController(context)
            bannerAdController2.setAdmobBannerAd(admobBannerId ?: "", bannerAdSize)
            bannerAdController2.setBannerHolder(this)

            nativeAdController = NativeAdController.getInstance(context, admobNativeId ?: "")
            nativeAdController?.setCallBackAd(bannerAdController2, adListener)
            nativeAdController?.populateNativeAd(this, nativeAdSize)
        }
    }
}