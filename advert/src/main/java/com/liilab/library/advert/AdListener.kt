package com.liilab.library.advert

open class AdListener {

    open fun onAdFailedToLoad(errorCode: Int, errorMessage: String?) {}
    open fun onAdClicked() {}
    open fun onAdClosed() {}
    open fun onAdLoaded() {}
    open fun onAdLeftApplication() {}
    open fun onAdOpened() {}
}