package com.liilab.library.advert.interstitial

import android.content.Context
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest

internal class AdmobInterstitial private constructor(
    val currentContext: Context,
    val id: String,
    val listener: com.liilab.library.advert.AdListener?): InterstitialAd {

    private var ad: com.google.android.gms.ads.InterstitialAd? = null

    override fun resume() {
    }

    override fun pause() {
    }

    override fun destroy() {
    }

    override fun loadAd() {
        ad = com.google.android.gms.ads.InterstitialAd(currentContext)
        ad?.adUnitId = id
        ad?.adListener = object : AdListener() {
            override fun onAdFailedToLoad(p0: Int) {
                listener?.onAdFailedToLoad(p0, null)
                super.onAdFailedToLoad(p0)
            }

            override fun onAdClosed() {
                super.onAdClosed()

                listener?.onAdClosed()
            }

            override fun onAdClicked() {
                super.onAdClicked()

                listener?.onAdClicked()
            }

            override fun onAdLoaded() {
                super.onAdLoaded()

                listener?.onAdLoaded()
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()

                listener?.onAdLeftApplication()
            }

            override fun onAdOpened() {
                super.onAdOpened()

                listener?.onAdOpened()
            }
        }

        val adRequestBuilder =
            AdRequest.Builder()

        // Optionally populate the ad request builder.
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        ad?.loadAd(adRequestBuilder.build())
    }

    override fun isLoaded(): Boolean {
        return ad?.isLoaded ?: false
    }

    override fun show() {
        ad?.show()
    }

    companion object {
        val TEST_INTERSTITIAL_AD = "ca-app-pub-3940256099942544/1033173712"
    }

    data class Builder(val currentContext: Context,
                       var id: String = TEST_INTERSTITIAL_AD,
                       var listener: com.liilab.library.advert.AdListener? = null) {
        fun id(id: String) = apply { this.id = id }
        fun listener(listener: com.liilab.library.advert.AdListener?) = apply { this.listener = listener }
        fun build() = AdmobInterstitial(currentContext, id, listener)
    }
}