package com.liilab.library.advert.util

class AppConstants {

    companion object {
        val ONE_MIN = 1 * 60 * 1000L
        val THREE_SECONDS = 3 * 1000L
        val FIVE_SECONDS = 5 * 1000L
        val TEN_SECONDS = 10 * 1000L
        val TWENTY_SECONDS = 20 * 1000L
        val THIRTY_SECONDS = 30 * 1000L
        val ONE_HOUR = 1 * 60 * 60 * 1000L
        val ONE_DAY = 24 * 60 * 60 * 1000L
    }
}