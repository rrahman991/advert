package com.liilab.library.advert

import android.content.Context
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.liilab.library.advert.banner.admob.AdmobBanner
import com.liilab.library.advert.banner.BannerAd
import com.liilab.library.advert.banner.Size
import com.liilab.library.advert.log.FirebaseAnalyticsLog
import com.liilab.library.advert.log.IdeLog
import com.liilab.library.advert.log.LogEvent
import com.liilab.library.advert.util.AppConstants
import com.liilab.library.advert.util.DeviceUtil
import com.liilab.library.advert.view.PosterAdView

internal class BannerAdController(mContext: Context) {

    private var bannerAd: BannerAd? = null

    private val mCurrentContext: Context
    private var mHandler: Handler? = null
    private var mRetryRunnable: Runnable? = null
    private var mRetryCount = 4

    @Volatile
    private var mRetryGivenTime: Long = 0
    var adFailedToShowListener: AdFailedToShowListener? = null
    private var bannerHolder: View? = null

    private val logEvent: LogEvent

    interface AdFailedToShowListener {
        fun onFailedToLoadAd()
    }

    open abstract class

    private fun init() {
        mRetryCount = 4
        mHandler = Handler()
        adFailedToShowListener = null
    }

    private fun onBannerAdFail() {
        if (null != adFailedToShowListener) {
            adFailedToShowListener!!.onFailedToLoadAd()
        }
    }

    fun onBannerDestroy() {
        try {
            bannerAd?.destroy()
            if (null != mRetryRunnable) {
                mHandler!!.removeCallbacks(mRetryRunnable)
            }
            mRetryRunnable = null
            mHandler = null
        } catch (e: Exception) {
        }
    }

    fun onBannerPause() {
        bannerAd?.pause()
    }

    fun onBannerResume() {
        bannerAd?.resume()
    }

    @Throws(Exception::class)
    fun setBannerHolder(view: View?) {
        if (view !is PosterAdView) {
            throw Exception("viewholder must be posteradview")
        }
        bannerHolder = view
    }

    private fun loadAdmobBannerAds(
        view: View?
    ) {
        try {
            val adholder = view as PosterAdView

            onResetBannerHolder(view)

            val height = (bannerAd as AdmobBanner)?.adSize?.getAdSize()?.getHeightInPixels(mCurrentContext)
            if(adholder.layoutParams == null) {
                adholder.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            }

            adholder.layoutParams.height = height

            adholder.setBackgroundColor(
                mCurrentContext.getResources().getColor(R.color.white)
            )

            adholder.addView(bannerAd?.adView())

            bannerAd?.loadAd()

        } catch (e: Exception) {
            // TODO: handle exception
            onBannerAdFail()
            e.printStackTrace()
        }
    }

    private fun onResetBannerHolder(view: View?) {
        try {
            (view as ViewGroup?)!!.removeAllViews()
            bannerAd?.destroy()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun loadRunnables() {
        if (mRetryRunnable == null) {
            mRetryRunnable = Runnable {
                mRetryCount--
                if (mRetryCount > 0) {
                    this@BannerAdController.loadAdmobBannerAds(bannerHolder)
                }
            }
        }
    }

    private fun retryAfterGivenTime() {
        loadRunnables()
        if (null != mHandler) {
            mRetryGivenTime = AppConstants.THIRTY_SECONDS
            if (mRetryCount > 0) {
                mHandler!!.postDelayed(mRetryRunnable, mRetryGivenTime)
            }
        } else {
            //TODO fix this one
        }
    }

    private fun onRequestBannerAds() {
        this@BannerAdController.loadAdmobBannerAds(bannerHolder)
    }

    @Throws(Exception::class)
    fun loadBannerAds() {
        onResetBannerHolder(bannerHolder)

        if (DeviceUtil.isNetworkPresent(mCurrentContext)) {
            if (null == bannerHolder || (bannerHolder !is FrameLayout)) {
                throw Exception("BannerHolder view is not a framelayout or AdNetworkShowListener is not set yet check setAdNetworkShowListener")
            } else {
                loadRunnables()
                onRequestBannerAds()
            }
        }
    }

    companion object {
        private val TAG = BannerAdController::class.java.name
    }

    fun setAdmobBannerAd(id: String = AdmobBanner.TEST_BANNER_AD, size: Int = Size.ADAPTIVE_SIZE, callback: AdListener? = null) {
        bannerAd?.destroy()

        bannerAd = AdmobBanner.Builder(mCurrentContext).id(id).adSize(size)
            .adFailedListener(object : AdListener() {
                override fun onAdFailedToLoad(errorCode: Int, errorMessage: String?) {
                    super.onAdFailedToLoad(errorCode, errorMessage)
                    logEvent.log("ERROR_CODE", errorCode.toString())

                    onBannerDestroy()
                    onBannerAdFail()
                    retryAfterGivenTime()

                    callback?.onAdFailedToLoad(errorCode, errorMessage)
                }

                override fun onAdClicked() {
                    super.onAdClicked()

                    callback?.onAdClicked()
                }

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()

                    callback?.onAdLeftApplication()
                }

                override fun onAdOpened() {
                    super.onAdOpened()

                    callback?.onAdOpened()
                }

                override fun onAdClosed() {
                    super.onAdClosed()

                    callback?.onAdClosed()
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()

                    callback?.onAdLoaded()
                }
            }).build()
    }

    init {
        mCurrentContext = mContext
        init()

        logEvent = FirebaseAnalyticsLog(mContext)
        //logEvent = IdeLog()
    }
}