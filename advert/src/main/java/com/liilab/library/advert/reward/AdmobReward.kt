package com.liilab.library.advert.reward

import android.content.Context
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener

internal class AdmobReward private constructor(
    val currentContext: Context,
    val id: String,
    val listener: RewardAd.RewardAdListener<RewardItem?>?): RewardAd {

    private var rewardedAd: RewardedVideoAd? = null

    override fun resume() {
        rewardedAd?.resume(currentContext)
    }

    override fun pause() {
        rewardedAd?.pause(currentContext)
    }

    override fun destroy() {
        rewardedAd?.destroy(currentContext)
    }

    override fun loadAd() {
        rewardedAd = MobileAds.getRewardedVideoAdInstance(currentContext)
        rewardedAd?.rewardedVideoAdListener = object: RewardedVideoAdListener {
            override fun onRewardedVideoAdClosed() {
                listener?.onAdClosed()
            }

            override fun onRewardedVideoAdLeftApplication() {
            }

            override fun onRewardedVideoAdLoaded() {
                listener?.onAdLoaded()
            }

            override fun onRewardedVideoAdOpened() {
            }

            override fun onRewardedVideoCompleted() {
            }

            override fun onRewarded(p0: RewardItem?) {
                listener?.onRewarded(p0)
            }

            override fun onRewardedVideoStarted() {
            }

            override fun onRewardedVideoAdFailedToLoad(p0: Int) {
                listener?.onAdFailedToLoad(p0, null)
            }
        }

        val adRequestBuilder =
            AdRequest.Builder()

        // Optionally populate the ad request builder.
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)

        rewardedAd?.loadAd(id, adRequestBuilder.build())
    }

    override fun isLoaded(): Boolean {
        return rewardedAd?.isLoaded ?: false
    }

    override fun show() {
        rewardedAd?.show()
    }

    companion object {
        val TEST_REWARDED_AD = "ca-app-pub-3940256099942544/5224354917"
    }

    data class Builder(val currentContext: Context,
                       var id: String = TEST_REWARDED_AD, var listener: RewardAd.RewardAdListener<RewardItem?>? = null) {
        fun id(id: String) = apply { this.id = id }
        fun listener(listener: RewardAd.RewardAdListener<RewardItem?>?) = apply { this.listener = listener }
        fun build() = AdmobReward(currentContext, id, listener)
    }
}