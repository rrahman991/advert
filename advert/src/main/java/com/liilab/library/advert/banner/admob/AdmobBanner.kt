package com.liilab.library.advert.banner.admob

import android.content.Context
import android.view.View
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.liilab.library.advert.banner.BannerAd
import com.liilab.library.advert.banner.Size

internal class AdmobBanner private constructor(
    val currentContext: Context,
    val adSize: AdmobBannerSize,
    val id: String,
    val adFailedListener: com.liilab.library.advert.AdListener?):
    BannerAd {

    private var adView: AdView? = null

    override fun resume() {
        adView?.resume()
    }

    override fun pause() {
        adView?.pause()
    }

    override fun destroy() {
        adView?.destroy()
        adView = null
    }

    override fun loadAd() {
        var adRequest: AdRequest? = null
        adRequest = AdRequest.Builder()
            .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
            .build()

        adView?.loadAd(adRequest)

    }

    override fun adView(): View? {
        if(adView == null) {
            adView = AdView(currentContext)
        }

        adView?.adSize = adSize.getAdSize()
        adView?.adListener = object : AdListener() {
            override fun onAdFailedToLoad(p0: Int) {
                adFailedListener?.onAdFailedToLoad(p0, null)

                super.onAdFailedToLoad(p0)
            }

            override fun onAdClosed() {
                super.onAdClosed()

                adFailedListener?.onAdClosed()
            }

            override fun onAdClicked() {
                super.onAdClicked()

                adFailedListener?.onAdClicked()
            }

            override fun onAdLoaded() {
                super.onAdLoaded()

                adFailedListener?.onAdLoaded()
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()

                adFailedListener?.onAdLeftApplication()
            }

            override fun onAdOpened() {
                super.onAdOpened()

                adFailedListener?.onAdOpened()
            }
        }
        adView?.adUnitId = id

        return adView
    }

    companion object {
        val TEST_BANNER_AD = "ca-app-pub-3940256099942544/6300978111"
    }

    data class Builder(
        var currentContext: Context,
        var adSize: Int = Size.ADAPTIVE_SIZE,
        var id: String = TEST_BANNER_AD,
        var adFailedListener: com.liilab.library.advert.AdListener? = null
    ) {
        fun adSize(adSize: Int) = apply { this.adSize = adSize }
        fun id(id: String) = apply { this.id = id }
        fun adFailedListener(adFailedListener: com.liilab.library.advert.AdListener?) = apply { this.adFailedListener = adFailedListener }
        fun build() = AdmobBanner(
            currentContext,
            AdmobBannerSize(
                currentContext,
                adSize
            ),
            id,
            adFailedListener
        )
    }
}