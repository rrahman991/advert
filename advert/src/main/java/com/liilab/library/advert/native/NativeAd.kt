package com.liilab.library.advert.native

import android.view.ViewGroup

interface NativeAd {

    fun populateNativeAd(viewHolder: ViewGroup, heightType: Int)
}