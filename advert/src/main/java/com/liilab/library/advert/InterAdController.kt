package com.liilab.library.advert

import android.app.Activity
import android.content.Context
import com.google.android.gms.ads.AdRequest
import com.liilab.library.advert.interstitial.AdmobInterstitial
import com.liilab.library.advert.interstitial.InterstitialAd
import com.liilab.library.advert.log.FirebaseAnalyticsLog
import com.liilab.library.advert.log.IdeLog
import com.liilab.library.advert.log.LogEvent
import java.util.*

class InterAdController(context: Context) {
    private var retryTimer: Timer? = null

    interface AdFailedToShowListener {
        fun onFailedToLoadAd()
    }

    private var interstitialAd: InterstitialAd? = null

    private val mCurrentContext: Context
    private var mAdFailedToShowListener: AdFailedToShowListener? = null

    private val logEvent: LogEvent

    private var admobInterId: String = AdmobInterstitial.TEST_INTERSTITIAL_AD

    init {
        mCurrentContext = context
        //logEvent = IdeLog()
        logEvent = FirebaseAnalyticsLog(context)

        try {
            admobInterId = context.getApplicationContext().getString(
                    context.getApplicationContext().getResources().getIdentifier(
                            "admob_interstitial_id", "string",
                            context.getApplicationContext().getPackageName()
                    )
            )
        } catch (e: Exception) {
        }
    }

    fun onRequestToLoadAd() {
        interstitialAd?.loadAd()
    }

    fun showInterstitialAd() {
        var adshown = false
        var admobloaded = false
        if (interstitialAd?.isLoaded() ?: false) {
            admobloaded = true
        }

        if (admobloaded) {
            interstitialAd?.show()
            adshown = true
        }

        if (!adshown) {
            onRequestToLoadAd()
        }
    }

    fun onDestroy() {
        interstitialAd?.destroy()
    }

    fun onPause() {
        interstitialAd?.pause()
    }

    fun onResume() {
        interstitialAd?.resume()
    }

    inner class MyTimerTask internal constructor(var runnable: Runnable?) :
        TimerTask() {
        override fun run() {
            if (runnable != null) {
                try {
                    (mCurrentContext as Activity?)!!.runOnUiThread(runnable)
                } catch (e: Exception) {
                }
            }
        }

    }

    private fun retryAfterGivenTime(
        context: Context,
        isGoogleAd: Boolean,
        givenTime: Int
    ) {
        val runnable = Runnable {
            try {
                onRequestToLoadAd()
            } catch (e: Exception) {
            }
        }
        startTimer(runnable, givenTime)
    }

    private fun startTimer(runnable: Runnable, time: Int) {
        try {
            if (retryTimer == null) {
                retryTimer = Timer("retryTimer", true)
            }
            val timerTask: TimerTask = MyTimerTask(runnable)
            retryTimer!!.schedule(timerTask, time.toLong())
        } catch (e: Exception) {
        }
    }

    fun setAdmobInterstitialAd() {
        interstitialAd = AdmobInterstitial.Builder(mCurrentContext).id(admobInterId).listener(object: AdListener() {

            override fun onAdClosed() {
                onRequestToLoadAd()
            }

            override fun onAdFailedToLoad(errorCode: Int, errorMessage: String?) {
                super.onAdFailedToLoad(errorCode, errorMessage)

                logEvent.log("ERROR_CODE", errorCode.toString())

                if (errorCode == AdRequest.ERROR_CODE_NO_FILL || errorCode == AdRequest.ERROR_CODE_NETWORK_ERROR || errorCode == AdRequest.ERROR_CODE_INTERNAL_ERROR
                ) {
                    retryAfterGivenTime(mCurrentContext, true, 30 * 1000)
                }
            }
        }).build()
    }

    companion object {
        private val TAG = InterAdController::class.java.name
    }
}