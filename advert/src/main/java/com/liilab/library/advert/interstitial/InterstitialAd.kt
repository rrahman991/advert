package com.liilab.library.advert.interstitial

interface InterstitialAd {

    fun resume()

    fun pause()

    fun destroy()

    fun loadAd()

    fun isLoaded(): Boolean

    fun show()
}