package com.liilab.library.advert.log

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

class FirebaseAnalyticsLog(private val context: Context): LogEvent {

    override fun log(key: String, value: String) {
        val params = Bundle()
        params.putString(key, value)

        FirebaseAnalytics.getInstance(context)
            .logEvent("ADVERB_ERROR", params)
    }
}