package com.liilab.library.advert

import android.content.Context
import android.view.ViewGroup
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.liilab.library.advert.native.AdmobNative
import com.liilab.library.advert.native.NativeAd
import com.liilab.library.advert.view.NativeAdHolder

internal class NativeAdController private constructor(context: Context, id: String) {

    private val currentContext: Context

    private var nativeAd: AdmobNative? = null

    init {
        currentContext = context

        setAdmobNativeAd(id)
    }

    private fun setAdmobNativeAd(id: String) {
        nativeAd = AdmobNative(currentContext, id, null)
    }

    fun setCallBackAd(bannerAdController: BannerAdController, callback: AdListener? = null) {
        nativeAd?.listener = object: AdListener() {
            override fun onAdFailedToLoad(errorCode: Int, errorMessage: String?) {
                super.onAdFailedToLoad(errorCode, errorMessage)

                bannerAdController.loadBannerAds()
            }

            override fun onAdClicked() {
                super.onAdClicked()

                callback?.onAdClicked()
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()

                callback?.onAdLeftApplication()
            }

            override fun onAdOpened() {
                super.onAdOpened()

                callback?.onAdOpened()
            }

            override fun onAdClosed() {
                super.onAdClosed()

                callback?.onAdClosed()
            }

            override fun onAdLoaded() {
                super.onAdLoaded()

                callback?.onAdLoaded()
            }
        }
    }

    fun populateNativeAd(viewHolder: ViewGroup, size: Int) {
        nativeAd?.populateNativeAd(viewHolder, size)
    }

    companion object {
        private var instance: NativeAdController? = null

        fun getInstance(context: Context, id: String): NativeAdController? {
            if(instance == null) {
                instance = NativeAdController(context.applicationContext, id)
            }

            return instance
        }
    }
}