package com.liilab.library.advert

import android.content.Context
import com.google.android.gms.ads.reward.RewardItem
import com.liilab.library.advert.log.FirebaseAnalyticsLog
import com.liilab.library.advert.log.IdeLog
import com.liilab.library.advert.log.LogEvent
import com.liilab.library.advert.reward.AdmobReward
import com.liilab.library.advert.reward.RewardAd

class RewardAdController(context: Context) {

    interface OnRewardedAdListener {
        fun onAdLoaded()
        fun onAdLoadFailed()
        fun onRewarded()
        fun onAdClosed()
    }

    private var rewardedVideoAd: RewardAd? = null

    fun onResume() {
        rewardedVideoAd?.resume()
    }

    fun onPause() {
        rewardedVideoAd?.pause()
    }

    fun onDestroy() {
        rewardedVideoAd?.destroy()
    }

    private val mCurrentContext: Context
    private var mOnRewardedAdListener: OnRewardedAdListener? = null

    private val logEvent: LogEvent

    private var admobRewardId: String = AdmobReward.TEST_REWARDED_AD

    init {
        mCurrentContext = context
        //logEvent = IdeLog()
        logEvent = FirebaseAnalyticsLog(context)

        try {
            admobRewardId = context.getApplicationContext().getString(
                    context.getApplicationContext().getResources().getIdentifier(
                            "admob_rewarded_ad_id", "string",
                            context.getApplicationContext().getPackageName()
                    )
            )
        } catch (e: Exception) {
        }
    }

    fun setOnRewardedAdListener(mOnRewardedAdListener: OnRewardedAdListener?) {
        this.mOnRewardedAdListener = mOnRewardedAdListener
    }

    private var loadedFailed = false

    /**
     * load rewarded video ad */
    fun onRequestToLoadAd() {
        loadedFailed = false
        rewardedVideoAd?.loadAd()
    }

    /**
     * finally show rewarded video ad */
    fun show() {
        rewardedVideoAd?.show()
    }

    fun isLoaded(): Boolean {
        return rewardedVideoAd?.isLoaded() ?: false
    }

    fun isLoadedFailed(): Boolean {
        return loadedFailed
    }

    fun setAdmobRewardAd() {
        loadedFailed = false

        rewardedVideoAd = AdmobReward.Builder(mCurrentContext).id(admobRewardId).listener(object: RewardAd.RewardAdListener<RewardItem?>() {
            override fun onRewarded(reward: RewardItem?) {
                super.onRewarded(reward)

                mOnRewardedAdListener?.onRewarded()
            }

            override fun onAdFailedToLoad(errorCode: Int, errorMessage: String?) {
                super.onAdFailedToLoad(errorCode, errorMessage)

                logEvent.log("ERROR_CODE", errorCode.toString())

                loadedFailed = true

                mOnRewardedAdListener?.onAdLoadFailed()
            }

            override fun onAdLoaded() {
                super.onAdLoaded()

                loadedFailed = false

                mOnRewardedAdListener?.onAdLoaded()
            }

            override fun onAdClosed() {
                super.onAdClosed()

                mOnRewardedAdListener?.onAdClosed()
            }
        }).build()
    }
}