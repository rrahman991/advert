package com.liilab.library.advert.banner.admob

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics
import com.google.android.gms.ads.AdSize
import com.liilab.library.advert.banner.Size

internal class AdmobBannerSize(private val currentContext: Context, private val size: Int) {

    fun getAdSize(): AdSize {
        when(size) {
            Size.LARGE_BANNER -> return AdSize.LARGE_BANNER
            Size.MEDIUM_RECTANGLE -> return AdSize.MEDIUM_RECTANGLE
            else -> return getAdaptiveBannerAdSize(currentContext)
        }
    }

    private fun getAdaptiveBannerAdSize(mCurrentContext: Context): AdSize {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        val display =
            (mCurrentContext as Activity).windowManager.defaultDisplay
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)
        val widthPixels = outMetrics.widthPixels.toFloat()
        val density = outMetrics.density
        val adWidth = (widthPixels / density).toInt()

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
            mCurrentContext,
            adWidth
        )
    }
}