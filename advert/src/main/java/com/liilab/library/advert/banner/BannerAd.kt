package com.liilab.library.advert.banner

import android.view.View

interface BannerAd {

    fun resume()

    fun pause()

    fun destroy()

    fun loadAd()

    fun adView(): View?
}