package com.liilab.library.advert.log

interface LogEvent {

    fun log(key: String, value: String)
}