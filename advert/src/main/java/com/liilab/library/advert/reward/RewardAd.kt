package com.liilab.library.advert.reward

import com.google.android.gms.ads.reward.RewardItem

interface RewardAd {

    fun resume()

    fun pause()

    fun destroy()

    fun loadAd()

    fun isLoaded(): Boolean

    fun show()

    open abstract class RewardAdListener<Reward> {
        open fun onAdFailedToLoad(errorCode: Int, errorMessage: String?) {}
        open fun onRewarded(reward: Reward) {}
        open fun onAdClosed() {}
        open fun onAdLoaded() {}
    }
}