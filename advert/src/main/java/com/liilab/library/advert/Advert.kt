package com.liilab.library.advert

import android.content.Context
import androidx.ads.identifier.AdvertisingIdClient
import androidx.ads.identifier.AdvertisingIdInfo
import com.google.android.gms.ads.MobileAds
import com.google.common.util.concurrent.FutureCallback
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.MoreExecutors
import org.checkerframework.checker.nullness.compatqual.NullableDecl


class Advert private constructor() {

    companion object {
        val TEST_APP_ID = "ca-app-pub-3940256099942544~3347511713"

        @Volatile
        private var advert: Advert? = null

        @JvmStatic
        fun getInstance(): Advert {
            if(advert == null) {
                synchronized(Advert::class.java) {
                    val ins = Advert()

                    advert = ins
                }
                //advert = Advert()
            }

            return advert!!
        }
    }

    init {
        //initAdmob(context)
    }

    //companion object: SingletonHolder<Advert, Context>(::Advert)

    private var advertId: String = ""

    private fun initAdmob(appContext: Context) {

        var admob_app_id = ""

        try {
            admob_app_id = appContext.getApplicationContext().getString(
                appContext.getApplicationContext().getResources().getIdentifier(
                    "admob_app_id", "string",
                    appContext.getApplicationContext().getPackageName()
                )
            )
        } catch (e: Exception) {
        }

        MobileAds.initialize(appContext, admob_app_id)

        determineAdvertisingInfo(appContext)
    }

    fun init(context: Context) {
        initAdmob(context)
    }

    fun getAdvertId(): String {
        return advertId
    }

    private fun determineAdvertisingInfo(mContext: Context) {
        try {
            Thread(Runnable {
                try {
                    if (AdvertisingIdClient.isAdvertisingIdProviderAvailable(
                            mContext.applicationContext
                        )
                    ) {
                        val advertisingIdInfoListenableFuture =
                            AdvertisingIdClient.getAdvertisingIdInfo(
                                mContext.applicationContext
                            )
                        Futures.addCallback(
                            advertisingIdInfoListenableFuture,
                            object : FutureCallback<AdvertisingIdInfo?> {
                                override fun onSuccess(@NullableDecl adInfo: AdvertisingIdInfo?) {
                                    if (null != adInfo) {
                                        advertId = adInfo.id
                                    }
                                }

                                override fun onFailure(t: Throwable) {}
                            },
                            MoreExecutors.directExecutor()
                        )
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
                try {
                    var advertising_id: String = advertId
                    if (advertising_id.length <= 0) {
                        val adInfo: OAAdvertisingIdClient.AdInfo =
                            OAAdvertisingIdClient.getAdvertisingIdInfo(mContext.applicationContext)
                        advertising_id = adInfo.id ?: ""

                        advertId = advertising_id
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }).start()
        } catch (e: java.lang.Exception) {
        }
    }
}