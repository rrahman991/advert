package com.liilab.library.advert.banner

class Size {

    companion object {
        val LARGE_BANNER = 1
        val MEDIUM_RECTANGLE = 2
        val ADAPTIVE_SIZE  = 0
    }
}