package com.liilab.library.advert.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.liilab.library.advert.R

class NativeAdHolder: FrameLayout {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView()
    }

    private lateinit var adProgress: ProgressBar
    private lateinit var adFrameLayout: FrameLayout

    private fun initView() {
        val layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        layoutInflater.inflate(R.layout.native_ad_holder, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        adProgress = findViewById(R.id.adProgress)
        adFrameLayout = findViewById(R.id.adFrameLayout)
    }

    fun adProgress() = adProgress
    fun adFrameLayout() = adFrameLayout
}