package com.liilab.library.advert.log

import android.util.Log

class IdeLog: LogEvent {
    override fun log(key: String, value: String) {
        Log.wtf("log", key + " " + value)
    }
}